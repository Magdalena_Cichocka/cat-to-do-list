import cat from "../../styles/cat.png";
import pencil from "../../styles/pencil.png";
import remove from "../../styles/remove.png";

const ListItem = ({ id, title, editItem, deleteItem }) => {
  return (
    <div className="list-item__wrapper">
      <div className="list-item__point">
        <img src={cat} alt="cat icon" />
      </div>
      <div className="list-item__text">{title}</div>
      <div className="list-item__action-icons">
        <button className="action-button edit" onClick={() => editItem(id)}>
          <img src={pencil} alt="pencil icon" />
        </button>
        <button className="action-button delete" onClick={() => deleteItem(id)}>
          <img src={remove} alt="trash can icon" />
        </button>
      </div>
    </div>
  );
};
export default ListItem;
