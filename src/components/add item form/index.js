import { nanoid } from "nanoid";

const AddItemForm = ({
  items,
  setItems,
  name,
  setName,
  isEditing,
  editId,
  setIsEditing,
  setEditId,
}) => {
  const handleChange = (e) => {
    setName(e.target.value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (name && isEditing) {
      setItems(
        items.map((item) => {
          if (item.id === editId) {
            const editedItem = { id: item.id, title: name };
            console.log(editedItem);
            return editedItem;
          } else return item;
        })
      );
      setEditId(null);
      setName("");
      setIsEditing(false);
    } else if (name) {
      let id = nanoid();
      const newItem = { id, title: name };
      console.log(newItem);
      setItems([...items, newItem]);
      setName("");
    } else {
      alert("empty value");
    }
  };

  return (
    <form>
      <label htmlFor="new-item">
        What has to be done, bought or remembered?
      </label>
      <div className="input-wrapper">
        <input
          id="new-item"
          type="text"
          value={name}
          onChange={handleChange}
        ></input>
        <button className="add-button" type="submit" onClick={handleSubmit}>
          Add
        </button>
      </div>
    </form>
  );
};
export default AddItemForm;
