import React, { useEffect, useState } from "react";
import AddItem from "../add item form";
import ListItem from "../list item";

const getLocalStorage = () => {
  let localItems = localStorage.getItem("items");
  if (localItems) {
    return JSON.parse(localStorage.getItem("items"));
  } else return [];
};

const List = () => {
  const [items, setItems] = useState(getLocalStorage());
  const [name, setName] = useState("");
  const [isEditing, setIsEditing] = useState(false);
  const [editId, setEditId] = useState(null);

  const deleteItem = (id) => {
    const newItems = items.filter((item) => item.id !== id);
    setItems(newItems);
  };

  const editItem = (id) => {
    setIsEditing(true);
    const forEdit = items.find((item) => item.id === id);
    setEditId(id);
    setName(forEdit.title);
  };

  const removeAll = () => {
    setItems([]);
  };

  useEffect(() => {
    localStorage.setItem("items", JSON.stringify(items));
  }, [items]);

  return (
    <div className="main-wrapper">
      <h1>Your favourite to-do list</h1>
      <AddItem
        items={items}
        setItems={setItems}
        name={name}
        setName={setName}
        isEditing={isEditing}
        editId={editId}
        setEditId={setEditId}
        setIsEditing={setIsEditing}
      />

      <div>
        {items.map((item) => {
          const { id, title } = item;
          return (
            <ListItem
              key={id}
              id={id}
              title={title}
              editItem={editItem}
              deleteItem={deleteItem}
            />
          );
        })}
      </div>
      {items.length > 0 && (
        <div className="remove-all-button__wrapper">
          <button className="remove-all-button" onClick={removeAll}>
            Remove all
          </button>
        </div>
      )}
    </div>
  );
};

export default List;
